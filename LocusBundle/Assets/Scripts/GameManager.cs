﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BundleSystem;

// A behaviour that is attached to a playable
public class GameManager : MonoBehaviour
{
  IEnumerator Start()
  {
      DontDestroyOnLoad(gameObject);
      BundleManager.ShowDebugGUI = true;
      BundleManager.LogMessages = true;

      yield return BundleManager.Initialize();

      BundleManager.LoadScene("LocalAssets", "LocalScene", UnityEngine.SceneManagement.LoadSceneMode.Single);
  }
}
