﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BundleSystem;

public class TitleScene : MonoBehaviour
{
  private GameObject cube;
  private GameObject sphere;

  [SerializeField]
  private List<Material> materials;
  private int index = 0;
  IEnumerator Start()
  {
    var manifestReq = BundleManager.GetManifest();
    yield return manifestReq;
    if (!manifestReq.Succeeded)
    {
      Debug.LogError(manifestReq.ErrorCode);
      yield break;
    }
    Debug.Log($"Need to download { BundleManager.GetDownloadSize(manifestReq.Result) * 0.000001f } mb");
    var downloadReq = BundleManager.DownloadAssetBundles(manifestReq.Result);
    yield return downloadReq;
    if (!downloadReq.Succeeded)
    {
      Debug.LogError($"test: {downloadReq.ErrorCode}");
      yield break;
    }
    var loadedCube = BundleManager.Load<GameObject>("RemoteCube", "Cube");
    var loadedSphere = BundleManager.Load<GameObject>("RemoteSphere", "Sphere");
    materials = new List<Material>(BundleManager.LoadAll<Material>("RemoteMaterials"));
    cube = BundleManager.Instantiate(loadedCube);
    sphere = BundleManager.Instantiate(loadedSphere);
    BundleManager.ReleaseObject(loadedCube);
    BundleManager.ReleaseObject(loadedSphere);
  }

  public void ChangeCubeColor()
  {
    if (materials.Count > 0)
    {
      cube.GetComponent<MeshRenderer>().material = materials[index];
      index++;
      if (index > materials.Count-1)
      {
        index = 0;
      }
    }
  }

  public void ChangeSphereColor()
  {
    if (materials.Count > 0)
    {
      sphere.GetComponent<MeshRenderer>().material = materials[index];
      index++;
      if (index > materials.Count-1)
      {
        index = 0;
      }
    }
  }
}
